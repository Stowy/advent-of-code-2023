use std::collections::VecDeque;

#[aoc(day4, part1)]
pub fn part1(input: &str) -> usize {
    let mut sum: usize = 0;
    for line in input.lines() {
        let numbers = line.split_once(": ").unwrap().1;
        let split_numbers = numbers.split_once(" | ").unwrap();
        let winning_numbers: Vec<u8> = split_numbers
            .0
            .split(' ')
            .filter_map(|num| num.parse().ok())
            .collect();
        let my_numbers: Vec<u8> = split_numbers
            .1
            .split(' ')
            .filter_map(|num| num.parse().ok())
            .collect();

        let matches: Vec<&u8> = my_numbers
            .iter()
            .filter(|num| winning_numbers.contains(num))
            .collect();

        let mut score = 0;
        for (i, _) in matches.iter().enumerate() {
            if i == 0 {
                score += 1;
            } else {
                score *= 2;
            }
        }

        sum += score;
    }

    sum
}

#[aoc(day4, part2)]
pub fn part2(input: &str) -> usize {
    let mut sum: usize = 0;
    let mut copy_queue: VecDeque<u32> = VecDeque::new();
    for line in input.lines() {
        let split_line = line.split_once(": ").unwrap();
        let numbers = split_line.1;
        let split_numbers = numbers.split_once(" | ").unwrap();
        let winning_numbers: Vec<u8> = split_numbers
            .0
            .split(' ')
            .filter_map(|num| num.parse().ok())
            .collect();
        let my_numbers: Vec<u8> = split_numbers
            .1
            .split(' ')
            .filter_map(|num| num.parse().ok())
            .collect();

        let match_count = my_numbers
            .iter()
            .filter(|num| winning_numbers.contains(num))
            .count();

        let current_copy = copy_queue.pop_front().unwrap_or(0) + 1;
        sum +=  current_copy as usize;

        for i in 0..match_count {
            if i < copy_queue.len() {
                copy_queue[i] += current_copy;
            } else {
                copy_queue.push_back(current_copy);
            }
        }
    }

    sum
}

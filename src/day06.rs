#[aoc(day6, part1)]
pub fn part1(input: &str) -> u32 {
    let mut lines = input.lines();
    let times: Vec<_> = lines
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|num| num.parse::<u32>().ok())
        .collect();
    let distances: Vec<_> = lines
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|num| num.parse::<u32>().ok())
        .collect();

    let mut product = 1;
    for (time, distance_to_beat) in times.iter().zip(distances.iter()) {
        let mut count = 0;
        for hold_duration in 1..*time {
            let remaining = time - hold_duration;
            let distance = hold_duration * remaining;
            if distance > *distance_to_beat {
                count += 1;
            }
        }
        product *= count;
    }

    product
}

#[aoc(day6, part2)]
pub fn part2(input: &str) -> u64 {
    let mut lines = input.lines();
    let time: u64 = lines
        .next()
        .unwrap()
        .split_whitespace()
        .skip(1)
        .collect::<String>()
        .parse()
        .unwrap();
    let distance_to_beat: u64 = lines
        .next()
        .unwrap()
        .split_whitespace()
        .skip(1)
        .collect::<String>()
        .parse()
        .unwrap();

    let mut count = 0;
    for hold_duration in 1..time {
        let remaining = time - hold_duration;
        let distance = hold_duration * remaining;
        if distance > distance_to_beat {
            count += 1;
        }
    }

    count
}

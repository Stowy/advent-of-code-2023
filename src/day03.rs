fn twod_iter<T: num_traits::int::PrimInt + std::iter::Step>(
    x_size: T,
    y_size: T,
) -> impl Iterator<Item = (T, T)> {
    (T::zero()..y_size).flat_map(move |y| (T::zero()..x_size).map(move |x: T| (x, y)))
}

fn twod_iter_start<T: num_traits::int::PrimInt + std::iter::Step>(
    x_start: T,
    x_end: T,
    y_start: T,
    y_end: T,
) -> impl Iterator<Item = (T, T)> {
    (y_start..y_end).flat_map(move |y| (x_start..x_end).map(move |x| (x, y)))
}

fn is_symbol(character: char) -> bool {
    (!character.is_numeric()) && (character != '.')
}

#[derive(PartialEq)]
struct NumberData {
    position: (usize, usize),
    width: usize,
    value: i32,
}

#[aoc(day3, part1)]
pub fn part1(input: &str) -> i32 {
    let mut sum = 0;
    let line_length = input.lines().next().unwrap().len();
    let lines_count = input.lines().count();
    let chars: Vec<char> = input.lines().flat_map(|line| line.chars()).collect();
    let get_at = |x: usize, y: usize| chars[y * line_length + x];

    let mut numbers: Vec<NumberData> = Vec::new();
    let mut parse_numbers_start = -1;
    let mut number_string_buffer = String::new();
    for (x, y) in twod_iter(line_length, lines_count) {
        let current_character = get_at(x, y);
        let is_number = current_character.is_numeric();
        let is_parsing_numbers = parse_numbers_start >= 0;
        if is_number {
            number_string_buffer.push(current_character);
        }

        if !is_parsing_numbers && is_number {
            parse_numbers_start = x as i32;
        }

        if is_parsing_numbers && !is_number {
            numbers.push(NumberData {
                position: (parse_numbers_start as usize, y),
                width: number_string_buffer.len(),
                value: number_string_buffer.parse().unwrap(),
            });
            number_string_buffer.clear();
            parse_numbers_start = -1;
        }
    }

    for number in &numbers {
        let int_width = number.width as i32;
        for (x, y) in twod_iter_start(-1, int_width + 1, -1, 2) {
            if x >= 0 && x < int_width && y == 0 {
                continue;
            }

            let wold_pos_x_int = x + number.position.0 as i32;
            let wold_pos_y_int = y + number.position.1 as i32;

            if wold_pos_x_int < 0
                || wold_pos_x_int >= line_length as i32
                || wold_pos_y_int < 0
                || wold_pos_y_int >= lines_count as i32
            {
                continue;
            }

            let wold_pos_x = (wold_pos_x_int) as usize;
            let wold_pos_y = (wold_pos_y_int) as usize;

            let surrounding_char = get_at(wold_pos_x, wold_pos_y);
            if is_symbol(surrounding_char) {
                sum += number.value;
                break;
            }
        }
    }

    sum
}

fn is_colliding(number: &NumberData, point: (usize, usize)) -> bool {
    let right_point = number.position.0 + number.width - 1;
    point.1 == number.position.1 && point.0 >= number.position.0 && point.0 <= right_point
}

#[aoc(day3, part2)]
pub fn part2(input: &str) -> i64 {
    let mut sum = 0;
    let line_length = input.lines().next().unwrap().len();
    let lines_count = input.lines().count();
    let chars: Vec<char> = input.lines().flat_map(|line| line.chars()).collect();
    let get_at = |x: usize, y: usize| chars[y * line_length + x];

    let mut numbers: Vec<NumberData> = Vec::new();
    let mut stars: Vec<(usize, usize)> = Vec::new();
    let mut parse_numbers_start = -1;
    let mut number_string_buffer = String::new();
    for (x, y) in twod_iter(line_length, lines_count) {
        let current_character = get_at(x, y);
        if current_character == '*' {
            stars.push((x, y));
        }

        let is_number = current_character.is_numeric();
        let is_parsing_numbers = parse_numbers_start >= 0;
        if is_number {
            number_string_buffer.push(current_character);
        }

        if !is_parsing_numbers && is_number {
            parse_numbers_start = x as i32;
        }

        if is_parsing_numbers && !is_number {
            numbers.push(NumberData {
                position: (parse_numbers_start as usize, y),
                width: number_string_buffer.len(),
                value: number_string_buffer.parse().unwrap(),
            });
            number_string_buffer.clear();
            parse_numbers_start = -1;
        }
    }

    let mut parts = Vec::new();
    for star in stars {
        for (x, y) in twod_iter_start(-1, 2, -1, 2) {
            if x == 0 && y == 0 {
                continue;
            }

            let wold_pos_x_int = x + star.0 as i32;
            let wold_pos_y_int = y + star.1 as i32;

            let wold_pos_x = (wold_pos_x_int) as usize;
            let wold_pos_y = (wold_pos_y_int) as usize;

            for number in &numbers {
                if parts.contains(&number) {
                    continue;
                }
                if number.value == 738 && wold_pos_y == 96 && y == 0 {
                    println!("Prout");
                }

                if is_colliding(number, (wold_pos_x, wold_pos_y)) {
                    parts.push(number);
                    break;
                }
            }
        }

        if parts.len() == 2 {
            sum += (parts[0].value as i64) * (parts[1].value as i64);
        }
        parts.clear();
    }

    sum
}

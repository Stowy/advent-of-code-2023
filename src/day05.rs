#[derive(Debug, Default, Clone)]
struct MapRange {
    source_range_start: i64,
    dest_range_start: i64,
    range_length: i64,
}

impl MapRange {
    fn map_value(&self, value: i64) -> Option<i64> {
        let source_range_end = self.source_range_start + self.range_length;
        if value < self.source_range_start || value >= source_range_end {
            None
        } else {
            let diff = self.dest_range_start - self.source_range_start;
            Some(value + diff)
        }
    }
}

#[derive(Debug, Default, Clone)]
struct Map {
    ranges: Vec<MapRange>,
}

impl Map {
    fn map_value(&self, value: i64) -> i64 {
        for map_range in &self.ranges {
            if let Some(mapped_value) = map_range.map_value(value) {
                return mapped_value;
            }
        }

        value
    }
}

const MAPS_COUNT: usize = 7;

#[aoc(day5, part1)]
pub fn part1(input: &str) -> i64 {
    let mut lines = input.lines();
    let seeds_line = lines.next().unwrap();
    let seeds_string = seeds_line.split_at(7).1;
    let seeds = seeds_string
        .split(' ')
        .map(|seed| seed.parse::<i64>().unwrap());

    // Skip empty line
    lines.next();
    let mut maps: [Map; MAPS_COUNT] = array_init::array_init(|_| Map::default());
    // There are only seven maps
    for map in &mut maps {
        let mut ranges: Vec<MapRange> = Vec::new();
        // Skip the map name
        lines.next();
        loop {
            let line = lines.next().unwrap_or_default();
            if line.is_empty() {
                break;
            }

            let mut range_split = line.split_whitespace();
            let dest_range_start: i64 = range_split.next().unwrap().parse().unwrap();
            let source_range_start: i64 = range_split.next().unwrap().parse().unwrap();
            let range_length: i64 = range_split.next().unwrap().parse().unwrap();

            ranges.push(MapRange {
                source_range_start,
                dest_range_start,
                range_length,
            });
        }

        *map = Map { ranges };
    }

    let mut locations = Vec::new();
    for seed in seeds {
        let mut value = seed;
        for map in &maps {
            value = map.map_value(value);
        }
        locations.push(value);
    }
    // println!("{:?}", seeds.map(|s| s.to_string()).collect::<Vec<_>>());

    *locations.iter().min().unwrap()
}

fn compute_location(maps: &[Map], seed: i64) -> i64 {
    let mut value = seed;
    for map in maps {
        value = map.map_value(value);
    }
    value
}

#[aoc(day5, part2)]
pub fn part2(input: &str) -> i64 {
    let mut lines = input.lines();
    let seeds_line = lines.next().unwrap();
    let seeds_string = seeds_line.split_at(7).1;
    let seeds_input = seeds_string
        .split(' ')
        .map(|seed| seed.parse::<i64>().unwrap());

    // Skip empty line
    lines.next();
    let mut maps: [Map; MAPS_COUNT] = array_init::array_init(|_| Map::default());
    for map in &mut maps {
        let mut ranges: Vec<MapRange> = Vec::new();
        // Skip the map name
        lines.next();
        loop {
            let line = lines.next().unwrap_or_default();
            if line.is_empty() {
                break;
            }

            let mut range_split = line.split_whitespace();
            let dest_range_start: i64 = range_split.next().unwrap().parse().unwrap();
            let source_range_start: i64 = range_split.next().unwrap().parse().unwrap();
            let range_length: i64 = range_split.next().unwrap().parse().unwrap();

            ranges.push(MapRange {
                source_range_start,
                dest_range_start,
                range_length,
            });
        }

        *map = Map { ranges };
    }

    let mut smallest_location = i64::MAX;
    for [seeds_start, seeds_length] in seeds_input.array_chunks() {
        let seeds_end = seeds_start + seeds_length;
        for seed in seeds_start..seeds_end {
            let location = compute_location(&maps, seed);
            smallest_location = smallest_location.min(location);
        }
    }

    smallest_location
}

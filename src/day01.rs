use std::str::Chars;

#[aoc(day1, part1)]
pub fn part1(input: &str) -> i32 {
    let mut sum: i32 = 0;
    for line in input.lines() {
        let mut first_char: char = 'a';
        let mut last_char: char = 'a';

        for char in line.chars() {
            if char.is_numeric() {
                if first_char == 'a' {
                    first_char = char;
                }
                last_char = char;
            }
        }

        let val_string = format!("{first_char}{last_char}");
        let val: i32 = val_string.parse().unwrap();
        sum += val;
    }

    sum
}

const NUMBER_TEXTS: [(&str, char); 9] = [
    ("one", '1'),
    ("two", '2'),
    ("three", '3'),
    ("four", '4'),
    ("five", '5'),
    ("six", '6'),
    ("seven", '7'),
    ("eight", '8'),
    ("nine", '9'),
];

pub fn get_num_char_from_string(chars: &mut Chars<'_>) -> Option<char> {
    for number_text in NUMBER_TEXTS {
        if chars.as_str().starts_with(number_text.0) {
            chars.next().unwrap();

            return Some(number_text.1);
        }
    }

    None
}

#[aoc(day1, part2)]
pub fn part2(input: &str) -> i32 {
    let mut sum: i32 = 0;
    for line in input.lines() {
        let mut first_char: char = 'a';
        let mut last_char: char = 'a';

        let mut chars = line.chars();

        loop {
            let result = get_num_char_from_string(&mut chars);
            if result.is_none() {
                let character = chars.next();
                if character.is_none() {
                    break;
                }

                let character = character.unwrap();
                if character.is_numeric() {
                    if first_char == 'a' {
                        first_char = character;
                    }
                    last_char = character;
                }
            } else {
                if first_char == 'a' {
                    first_char = result.unwrap();
                }
                last_char = result.unwrap();
            }
        }

        let val_string = format!("{first_char}{last_char}");
        let val: i32 = val_string.parse().unwrap();
        sum += val;
    }

    sum
}

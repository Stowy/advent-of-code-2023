const MAX_RED: i32 = 12;
const MAX_GREEN: i32 = 13;
const MAX_BLUE: i32 = 14;

#[aoc(day2, part1)]
pub fn part1(input: &str) -> i32 {
    let mut sum: i32 = 0;
    for line in input.lines() {
        let splitted = line.split_once(": ").unwrap();
        let game_id_string = splitted.0.split_once(' ').unwrap();
        let game_id: i32 = game_id_string.1.parse().unwrap();
        let sets = splitted.1.split("; ");
        let mut is_possible = true;

        for set in sets {
            let showings = set.split(", ");
            let mut tupple_showings = (0, 0, 0);
            for showing in showings {
                let split_showing = showing.split_once(' ').unwrap();
                match split_showing.1 {
                    "red" => tupple_showings.0 = split_showing.0.parse().unwrap(),
                    "green" => tupple_showings.1 = split_showing.0.parse().unwrap(),
                    "blue" => tupple_showings.2 = split_showing.0.parse().unwrap(),
                    _ => panic!("Invalid color provided: {}", split_showing.1),
                }
            }

            if tupple_showings.0 > MAX_RED
                || tupple_showings.1 > MAX_GREEN
                || tupple_showings.2 > MAX_BLUE
            {
                is_possible = false;
                break;
            }
        }

        if is_possible {
            sum += game_id;
        }
    }
    sum
}


#[aoc(day2, part2)]
pub fn part2(input: &str) -> i32 {
    let mut sum: i32 = 0;
    for line in input.lines() {
        let splitted = line.split_once(": ").unwrap();
        let sets = splitted.1.split("; ");
        let mut min_inventory = (0, 0, 0);        

        for set in sets {
            let showings = set.split(", ");
            let mut tupple_showings = (0, 0, 0);
            for showing in showings {
                let split_showing = showing.split_once(' ').unwrap();
                match split_showing.1 {
                    "red" => tupple_showings.0 = split_showing.0.parse().unwrap(),
                    "green" => tupple_showings.1 = split_showing.0.parse().unwrap(),
                    "blue" => tupple_showings.2 = split_showing.0.parse().unwrap(),
                    _ => panic!("Invalid color provided: {}", split_showing.1),
                }
            }

            min_inventory.0 = tupple_showings.0.max(min_inventory.0);
            min_inventory.1 = tupple_showings.1.max(min_inventory.1);
            min_inventory.2 = tupple_showings.2.max(min_inventory.2);
        }

        let power = min_inventory.0 * min_inventory.1 * min_inventory.2;
        sum += power;
    }
    sum
}

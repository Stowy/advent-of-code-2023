use std::cmp::Ordering;

const CARDS_COUNT: usize = 13;
// const CARDS: [char; 13] = [
//     'A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2',
// ];

const CARD_STRENGTHS: [i32; 35] = [
//  2, 3, 4, 5, 6, 7, 8, 9,                             A,  b   c   d   e   f   g   h   i  J  K    l   m   n   o   p   Q   r   s   T
    0, 1, 2, 3, 4, 5, 6, 7, -1, -1, -1, -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, 9, 11, -1, -1, -1, -1, -1, 10, -1, -1, 8,
];

fn get_idx(card: char) -> usize {
    card as usize - '2' as usize
}

fn compare_strength(left: char, right: char) -> Ordering {
    CARD_STRENGTHS[get_idx(left)].cmp(&CARD_STRENGTHS[get_idx(right)])
}

#[derive(Debug, Copy, Clone)]
enum HandType {
    FiveOfAKind = 6,
    FourOfAKind = 5,
    FullHouse = 4,
    ThreeOfAKind = 3,
    TwoPair = 2,
    OnePair = 1,
    HighCard = 0,
}

#[derive(Debug)]
struct Hand {
    cards: [char; 5],
    hand_type: HandType,
    bid: u32,
}

impl Hand {
    fn new(cards: [char; 5], bid: u32) -> Self {
        let mut cards_count = [0; CARDS_COUNT];
        for char in cards {
            let index = CARD_STRENGTHS[get_idx(char)];
            if index == -1 {
                panic!("Bad strength {}", char);
            }
            cards_count[index as usize] += 1;
        }

        let hand_type = if cards_count.iter().any(|&c| c == 5) {
            HandType::FiveOfAKind
        } else if cards_count.iter().any(|&c| c == 4) {
            HandType::FourOfAKind
        } else if cards_count.iter().any(|&c| c == 3) {
            if cards_count.iter().any(|&c| c == 2) {
                HandType::FullHouse
            } else {
                HandType::ThreeOfAKind
            }
        } else if cards_count.iter().filter(|&&c| c == 2).count() == 2 {
            HandType::TwoPair
        } else if cards_count.iter().filter(|&&c| c == 2).count() == 1 {
            HandType::OnePair
        } else if cards_count.iter().filter(|&&c| c == 1).count() == 5 {
            HandType::HighCard
        } else {
            panic!("Bad hand: {:?}", cards)
        };

        Hand {
            cards,
            hand_type,
            bid,
        }
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let hand_type_cmp = (self.hand_type as usize).cmp(&(other.hand_type as usize));
        if hand_type_cmp.is_ne() {
            return hand_type_cmp;
        }

        for (&card, &other_card) in self.cards.iter().zip(other.cards.iter()) {
            let card_cmp = compare_strength(card, other_card);
            if card_cmp.is_ne() {
                return card_cmp;
            }
        }

        Ordering::Equal
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        (self.cards, self.bid) == (other.cards, other.bid)
    }
}

impl Eq for Hand {}

#[aoc(day7, part1)]
pub fn part1(input: &str) -> u32 {
    let mut hands = input
        .lines()
        .map(|line| line.split_once(' ').unwrap())
        .map(|h| (h.0, h.1.parse::<u32>().unwrap()))
        .map(|h| {
            let mut card = [' '; 5];
            for (i, c) in h.0.chars().enumerate() {
                card[i] = c;
            }
            Hand::new(card, h.1)
        })
        .collect::<Vec<_>>();
    hands.sort();
    hands
        .iter()
        .enumerate()
        .fold(0, |acc, (i, h)| acc + h.bid * (i as u32 + 1))
}
